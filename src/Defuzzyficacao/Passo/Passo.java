package Defuzzyficacao.Passo;

public class Passo {
	
	public Double Pouco(Double mDeX) {
		return 4 - (mDeX * (4.0 - 1.0));
	}
	
	public Double Longo(Double mDeX) {
		return (mDeX * (4.0 - 2.0)) + 2;
	}

}
