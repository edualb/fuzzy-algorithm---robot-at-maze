package InferenciaNebulosa;

import java.util.Random;

/**
 * Regras Nebulosas
 * @author Eduardo
 * 
 * 	Regra1 = SE sensor baixo = DISTANTE E sensor cima Distante ENT�O passos cima = POUCO OU passos baixo = POUCO;
	Regra2 = inferenciaNebulosa.RegraDireitaDistanteEEsquerdaDistanteEBaixoDistanteECimaDistante(DireitaDistante, EsquerdaDistante, BaixoDistante, CimaDistante);
	Regra3 = inferenciaNebulosa.RegraObstaculoDiretaDistante(DireitaDistante);
	Regra4 = inferenciaNebulosa.RegraObstaculoDiretaMedio(DireitaMedio);
	Regra5 = inferenciaNebulosa.RegraObstaculoDiretaPertoEObstaculoBaixoMedio(DireitaPerto, BaixoMedio);
	Regra6 = inferenciaNebulosa.RegraObstaculoDiretaPertoEObstaculoBaixoDistanteEObstaculoCimaPerto(DireitaPerto, BaixoDistante, CimaPerto);
	Regra7 = inferenciaNebulosa.RegraObstaculoDiretaMedioEObstaculoBaixoMedioEObstaculoCimaDistante(DireitaMedio, BaixoMedio, CimaDistante);
	Regra8 = inferenciaNebulosa.RegraObstaculoDiretaPertoEObstaculoBaixoPertoEObstaculoCimaMedio(DireitaPerto, BaixoPerto, CimaMedio);
	Regra9 = inferenciaNebulosa
					.RegraObstaculoDiretaPertoEObstaculoBaixoPertoEObstaculoCimaMedioEObstaculoEsquerdaDistante(DireitaPerto, BaixoPerto, CimaMedio, EsquerdaDistante);
	Regra10 = inferenciaNebulosa
					.RegraObstaculoDiretaPertoEObstaculoBaixoPertoEObstaculoCimaMedioEObstaculoEsquerdaMedio(DireitaPerto, BaixoPerto, CimaMedio, EsquerdaMedio);
	Regra11 = inferenciaNebulosa.RegraObstaculoDireitaPertoEObstaculoBaixoMedioPreferenciaEsquerda(DireitaPerto, BaixoMedio);
	Regra12 = inferenciaNebulosa.RegraObstaculoEsquerdaMedioEObstaculoDireitaMedio(EsquerdaMedio, DireitaMedio);
	Regra13 = inferenciaNebulosa.RegraObstaculoDiretaPertoEObstaculoBaixoPertoEObstaculoEsquerdaPerto(DireitaPerto, EsquerdaPerto, BaixoPerto);
	Regra14 = inferenciaNebulosa.RegraObstaculoDireitaPertoEObstaculoBaixoMedioPreferenciaDireita(DireitaPerto, BaixoMedio);
	Regra15 = inferenciaNebulosa.RegraObstaculoDiretaDistanteESensorEsquerdaPerto(DireitaDistante, EsquerdaPerto);
	Regra16 = inferenciaNebulosa.RegraObstaculoDiretaPertoESensorBaixoPertoESensorCimaDistante(DireitaPerto, BaixoPerto, CimaDistante);
	Regra17 = inferenciaNebulosa.RegraObstaculoDiretaDistanteESensorBaixoPerto(DireitaDistante, BaixoPerto);
	Regra18 = inferenciaNebulosa.RegraObstaculoCimaPerto(CimaPerto);
	Regra19 = inferenciaNebulosa.RegraObstaculoEsquerdaPertoPrioridadeDireita(EsquerdaPerto);
	Regra20 = inferenciaNebulosa.RegraObstaculoBaixoPerto(BaixoPerto);
	Regra21 = inferenciaNebulosa.RegraObstaculoDiretaPertoEObstaculoCimaMedio(CimaMedio, DireitaPerto);
	Regra22 = inferenciaNebulosa.RegraObstaculoDiretaDistanteEObstaculoEsquerdaDistante(EsquerdaDistante, DireitaDistante);
 *
 */

public class InferenciaNebulosa {
	
	public Double[] RegraDireitaDistanteEEsquerdaDistanteEBaixoDistanteECimaDistante(Double obstaculoDireita, Double obstaculoEsquerda, 
	  	Double obstaculoBaixo, Double obstaculoCima) {//1
	  		Double menor = obstaculoDireita;
			Double[] regra = new Double[3];
			Double direcao = geraAleatorio(2, 1) == 2 ? 2.0 : 1.0;
			
			if (direcao.equals(2.0)) {
				regra[1] = 6.0;
			} else {
				regra[1] = 8.0;
			}
			
			if (obstaculoEsquerda < menor) {
				menor = obstaculoEsquerda;
			} else if (obstaculoBaixo < menor) {
				menor = obstaculoBaixo;
			} else if (obstaculoCima < menor) {
				menor = obstaculoCima;
			}
	
			regra[0] = menor;
			regra[2] = direcao; //Cima ou baixo;
			return regra;
	}
	
	public Double[] RegraBaixoDistanteECimaDistante(Double obstaculoBaixo, Double obstaculoCima) {//2
		Double[] regra = new Double[3];
		Double direcao = geraAleatorio(3, 2) == 2 ? 2.0 : 3.0;
		
		if (direcao.equals(2.0)) {
			regra[1] = 5.0;
		} else {
			regra[1] = 3.0;
		}
		
		regra[0] = obstaculoBaixo > obstaculoCima ? obstaculoCima : obstaculoBaixo;
		regra[2] = direcao; //Baixo Ou Cima
		return regra;
	}
	
	public Double[] RegraObstaculoDiretaDistante(Double obstaculoDireita) { //3
		Double[] regra = new Double[3];
		regra[0] = obstaculoDireita;
		regra[1] = 8.0; // Valor da prioridade
		regra[2] = 1.0; //Direita
		return regra;
	}
	
	public Double[] RegraObstaculoDiretaMedio(Double obstaculoDireita) { //4
		Double[] regra = new Double[3];
		regra[0] = obstaculoDireita;
		regra[1] = 7.0; // Valor da prioridade
		regra[2] = 1.0; //Direita
		return regra;
	}
	
	public Double[] RegraObstaculoDiretaPertoEObstaculoBaixoMedio(Double obstaculoDireita, Double obstaculoBaixo) {//5
		Double[] regra = new Double[3];
		regra[0] = obstaculoDireita < obstaculoBaixo ? obstaculoDireita : obstaculoBaixo;
		regra[1] = 5.0; // Valor da prioridade
		regra[2] = 2.0; //Baixo
		return regra;
	}
	
	public Double[] RegraObstaculoDiretaMedioEObstaculoBaixoMedioEObstaculoCimaDistante(Double obstaculoDireita, Double obstaculoBaixo, Double obstaculoCima) {//REVER regra 6 e 7
		Double menor = obstaculoDireita;
		Double[] regra = new Double[3];
		Double direcao = geraAleatorio(4, 3) == 4 ? 4.0 : 3.0;
		
		if (obstaculoDireita > obstaculoBaixo) {
			menor = obstaculoBaixo;
		} else if (obstaculoDireita > obstaculoBaixo) {
			menor = obstaculoBaixo;
		}
		
		if (direcao.equals(3.0)) {
			regra[1] = 4.0;
		} else {
			regra[1] = 3.5;
		}
		
		regra[0] = menor;
		regra[2] = direcao; //Cima Ou Esquerda
		return regra;
	}
	
	public Double[] RegraObstaculoDiretaPertoEObstaculoBaixoDistanteEObstaculoCimaPerto(Double obstaculoDireita, Double obstaculoBaixo, Double obstaculoCima) {//REVER regra 6 e 7
		Double menor = obstaculoDireita;
		Double[] regra = new Double[3];
		Double direcao = geraAleatorio(4, 3) == 4 ? 4.0 : 2.0;
		
		if (obstaculoDireita > obstaculoBaixo) {
			menor = obstaculoBaixo;
		} else if (obstaculoDireita > obstaculoCima) {
			menor = obstaculoCima;
		}
		
		if (direcao.equals(2.0)) {
			regra[1] = 5.0;
		} else {
			regra[1] = 3.5;
		}
		
		regra[0] = menor;
		regra[2] = direcao; //Baixo
		return regra;
	}
	
	public Double[] RegraObstaculoDiretaPertoEObstaculoBaixoPertoEObstaculoCimaMedio(Double obstaculoDireita, Double obstaculoBaixo, Double obstaculoCima) { //8
		Double menor = obstaculoDireita;
		Double[] regra = new Double[3];
		Double direcao = geraAleatorio(4, 3) == 4 ? 4.0 : 3.0;
		
		if (obstaculoDireita > obstaculoBaixo) {
			menor = obstaculoBaixo;
		} else if (obstaculoDireita > obstaculoBaixo) {
			menor = obstaculoBaixo;
		}
		
		if (direcao.equals(3.0)) {
			regra[1] = 3.0;
		} else {
			regra[1] = 3.5;
		}
		
		regra[0] = menor;
		regra[2] = direcao; //Cima Ou Esquerda
		return regra;
	}
	
	public Double[] RegraObstaculoDiretaPertoEObstaculoBaixoPertoEObstaculoCimaMedioEObstaculoEsquerdaDistante(Double obstaculoDireita, Double obstaculoBaixo, 
			Double obstaculoCima, Double obstaculoEsquerda) { //9
		Double menor = obstaculoDireita;
		Double[] regra = new Double[3];
		
		if (obstaculoDireita > obstaculoBaixo) {
			menor = obstaculoBaixo;
		} else if (obstaculoDireita > obstaculoBaixo) {
			menor = obstaculoBaixo;
		} else if (obstaculoDireita > obstaculoEsquerda) {
			menor = obstaculoEsquerda;
		}
		
		regra[0] = menor;
		regra[1] = 3.5; // Valor da prioridade
		regra[2] = 4.0; //Esquerda
		
		return regra;
	}
	
	public Double[] RegraObstaculoDiretaPertoEObstaculoBaixoPertoEObstaculoCimaMedioEObstaculoEsquerdaMedio(Double obstaculoDireita, Double obstaculoBaixo, 
			Double obstaculoCima, Double obstaculoEsquerda) {//10
		Double menor = obstaculoDireita;
		Double[] regra = new Double[3];
		
		if (obstaculoDireita > obstaculoBaixo) {
			menor = obstaculoBaixo;
		} else if (obstaculoDireita > obstaculoBaixo) {
			menor = obstaculoBaixo;
		} else if (obstaculoDireita > obstaculoEsquerda) {
			menor = obstaculoEsquerda;
		}
		
		regra[0] = menor;
		regra[1] = 2.5; // Valor da prioridade
		regra[2] = 4.0; //Esquerda
		
		return regra;
	}
	
	public Double[] RegraObstaculoDireitaPertoEObstaculoBaixoMedioPreferenciaEsquerda(Double obstaculoDireita, Double obstaculoBaixo) { //11
		Double[] regra = new Double[3];
		
		regra[0] = obstaculoDireita < obstaculoBaixo ? obstaculoDireita : obstaculoBaixo;
		regra[1] = 2.5; // Valor da prioridade
		regra[2] = 4.0; //Esquerda
		
		return regra;
	}
	
	public Double[] RegraObstaculoEsquerdaMedioEObstaculoDireitaMedio(Double obstaculoEsquerda, Double obstaculoDireita) { //12
		Double[] regra = new Double[3];
		
		regra[0] = obstaculoDireita < obstaculoEsquerda ? obstaculoDireita : obstaculoEsquerda;
		regra[1] = 5.0; // Valor da prioridade
		regra[2] = 2.0; //Baixo
		
		return regra;
	}
	
	public Double[] RegraObstaculoDiretaPertoEObstaculoBaixoPertoEObstaculoEsquerdaPerto(Double obstaculoDireita, Double obstaculoEsquerda, Double obstaculoBaixo) { //13
		Double menor = obstaculoDireita;
		Double[] regra = new Double[3];
		
		if (obstaculoDireita > obstaculoBaixo) {
			menor = obstaculoBaixo;
		} else if (obstaculoDireita > obstaculoEsquerda) {
			menor = obstaculoEsquerda;
		}
		
		regra[0] = menor;
		regra[1] = 4.0; // Valor da prioridade
		regra[2] = 3.0; //Cima
		return regra;
	}
	
	public Double[] RegraObstaculoDireitaPertoEObstaculoBaixoMedioPreferenciaDireita(Double obstaculoDireita, Double obstaculoBaixo) { //14
		Double[] regra = new Double[3];
		
		regra[0] = obstaculoDireita < obstaculoBaixo ? obstaculoDireita : obstaculoBaixo;
		regra[1] = 7.0; // Valor da prioridade
		regra[2] = 1.0; //Direita
		
		return regra;
	}
	
	public Double[] RegraObstaculoEsquerdaPerto(Double obstaculoEsquerda) { //15
		Double[] regra = new Double[3];
		regra[0] = obstaculoEsquerda;
		regra[1] = 5.0; // Valor da prioridade
		regra[2] = 2.0; //Baixo
		return regra;
	}
	
	public Double[] RegraObstaculoDiretaDistanteESensorEsquerdaPerto(Double obstaculoDireita, Double obstaculoEsquerda) { //16
		Double[] regra = new Double[3];
		regra[0] = obstaculoDireita < obstaculoEsquerda ? obstaculoDireita : obstaculoEsquerda;;
		regra[1] = 8.0; // Valor da prioridade
		regra[2] = 1.0; //Direita
		return regra;
	}
	
	public Double[] RegraObstaculoDiretaPertoESensorBaixoPertoESensorCimaDistante(Double obstaculoDireita, Double obstaculoBaixo, Double obstaculoCima) { //17
		Double menor = obstaculoDireita;
		Double[] regra = new Double[3];
		Double direcao = geraAleatorio(4, 3) == 4 ? 4.0 : 3.0;
		
		if (obstaculoDireita > obstaculoBaixo) {
			menor = obstaculoBaixo;
		} else if (obstaculoDireita > obstaculoCima) {
			menor = obstaculoCima;
		}
		
		if (direcao.equals(3.0)) {
			regra[1] = 4.0;
		} else {
			regra[1] = 3.5;
		}
		
		regra[0] = menor;
		regra[2] = direcao; //Cima
		return regra;
	}
	
	public Double[] RegraObstaculoDiretaDistanteESensorBaixoPerto(Double obstaculoBaixo, Double obstaculoDireita) { //18
		Double[] regra = new Double[3];
		regra[0] = obstaculoDireita < obstaculoBaixo ? obstaculoDireita : obstaculoBaixo;;
		regra[1] = 8.0; // Valor da prioridade
		regra[2] = 1.0; //Direita
		return regra;
	}
	
	public Double[] RegraObstaculoCimaPerto(Double obstaculoCima) { //19
		Double[] regra = new Double[3];
		regra[0] = obstaculoCima;
		regra[1] = 5.0; // Valor da prioridade
		regra[2] = 2.0; //Baixo
		return regra;
	}
	
	public Double[] RegraObstaculoEsquerdaPertoPrioridadeDireita(Double obstaculoEsquerda) { //20
		Double[] regra = new Double[3];
		regra[0] = obstaculoEsquerda;
		regra[1] = 8.0; // Valor da prioridade
		regra[2] = 1.0; //Direita
		return regra;
	}
	
	public Double[] RegraObstaculoBaixoPerto(Double obstaculoBaixo) { //21
		Double[] regra = new Double[3];
		regra[0] = obstaculoBaixo;
		regra[1] = 4.0; // Valor da prioridade
		regra[2] = 3.0; //Direita
		return regra;
	}
	
	public Double[] RegraObstaculoDiretaPertoEObstaculoCimaMedio(Double obstaculoCima, Double obstaculoDireita) { //22
		Double[] regra = new Double[3];
		regra[0] = obstaculoDireita < obstaculoCima ? obstaculoDireita : obstaculoCima;;
		regra[1] = 7.0; // Valor da prioridade
		regra[2] = 1.0; //Direita
		return regra;
	}
	
	public Double[] RegraObstaculoDiretaDistanteEObstaculoEsquerdaDistante(Double obstaculoEsquerda, Double obstaculoDireita) { //22
		Double[] regra = new Double[3];
		regra[0] = obstaculoDireita < obstaculoEsquerda ? obstaculoDireita : obstaculoEsquerda;;
		regra[1] = 7.0; // Valor da prioridade
		regra[2] = 1.0; //Direita
		return regra;
	}
	
	private static int geraAleatorio(int max, int min) {
        Random random = new Random();
        return (random.nextInt(max - (min - 1)) + min);
    }

}
