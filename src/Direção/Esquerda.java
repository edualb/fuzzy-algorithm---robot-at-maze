package Dire��o;

public class Esquerda implements DirecaoPadrao{

	Double determiante;
	
	public Esquerda() {
		setDetermiante(0.0);
	}

	public Double getDetermiante() {
		return determiante;
	}

	public void setDetermiante(Double determiante) {
		this.determiante = determiante;
	}

	@Override
	public void somaDeterminanteDeDirecao(Double valor) {
		this.determiante += valor;
	}
	
}
