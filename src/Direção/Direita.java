package Dire��o;

public class Direita implements DirecaoPadrao{
	
	Double determiante;
	
	public Direita() {
		setDetermiante(0.0);
	}

	public Double getDetermiante() {
		return determiante;
	}

	public void setDetermiante(Double determiante) {
		this.determiante = determiante;
	}

	@Override
	public void somaDeterminanteDeDirecao(Double valor) {
		this.determiante += valor;
	}

}
