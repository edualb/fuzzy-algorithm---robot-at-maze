package LabirintoMain;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import Defuzzyficacao.Passo.Passo;
import Dire��o.Baixo;
import Dire��o.Cima;
import Dire��o.Direita;
import Dire��o.Esquerda;
import Fuzzyficacao.Obstaculo.Obstaculo;
import InferenciaNebulosa.InferenciaNebulosa;
import Sensores.SensorBaixo;
import Sensores.SensorCima;
import Sensores.SensorDireita;
import Sensores.SensorEsquerda;

public class LabirintoMain {
	
	private static Scanner scanner;
	static ArrayList<Double[]> deffuzzyficadores = new ArrayList<Double[]>();
	static Boolean achouSaida = false;
	static String labirinto[][];

	static SensorDireita sensorDireita;
	static SensorCima sensorCima;
	static SensorBaixo sensorBaixo;
	static SensorEsquerda sensorEsquerda;

	public static void main(String[] args) {
		scanner = new Scanner(System.in);
		Obstaculo fuzzyficador = new Obstaculo();
		Passo defuzzyficador = new Passo();
		InferenciaNebulosa inferenciaNebulosa = new InferenciaNebulosa();
		ArrayList<Double[]> inferenciaNebulosaList = new ArrayList<Double[]>();
		Double somaDefuzzyficadores = 0.0, somaFuzzyficadores = 0.0, qtePassos = 0.0;
		
		System.out.println("Digite o n�mero da linha da sa�da:");
		int saida = scanner.nextInt();
		
		labirinto = preencheLabirinto(saida); 
		
		sensorDireita = new SensorDireita(labirinto);
		sensorCima = new SensorCima(labirinto);
		sensorBaixo = new SensorBaixo(labirinto);
		sensorEsquerda= new SensorEsquerda(labirinto);
		
		Direita direcaoDireita = new Direita();
		Baixo direcaoBaixo = new Baixo();
		Cima direcaoCima = new Cima();
		Esquerda direcaoEsquerda = new Esquerda();
		
		imprimeLabirinto(labirinto);
		
		while(!achouSaida) {
		
			Double DireitaPerto = fuzzyficador.Perto(sensorDireita.identificaObstaculo());
			Double DireitaMedio = fuzzyficador.Medio(sensorDireita.identificaObstaculo());
			Double DireitaDistante = fuzzyficador.Distante(sensorDireita.identificaObstaculo());
			
			Double BaixoPerto = fuzzyficador.Perto(sensorBaixo.identificaObstaculo());
			Double BaixoMedio = fuzzyficador.Medio(sensorBaixo.identificaObstaculo());
			Double BaixoDistante = fuzzyficador.Distante(sensorBaixo.identificaObstaculo());
			
			Double CimaPerto = fuzzyficador.Perto(sensorCima.identificaObstaculo());
			Double CimaMedio = fuzzyficador.Medio(sensorCima.identificaObstaculo());
			Double CimaDistante = fuzzyficador.Distante(sensorCima.identificaObstaculo());
			
			Double EsquerdaPerto = fuzzyficador.Perto(sensorEsquerda.identificaObstaculo());
			Double EsquerdaMedio = fuzzyficador.Medio(sensorEsquerda.identificaObstaculo());
			Double EsquerdaDistante = fuzzyficador.Distante(sensorEsquerda.identificaObstaculo());
			
			Double[] Regra1 = inferenciaNebulosa.RegraBaixoDistanteECimaDistante(BaixoDistante, CimaDistante);
			Double[] Regra2 = inferenciaNebulosa.RegraDireitaDistanteEEsquerdaDistanteEBaixoDistanteECimaDistante(DireitaDistante, EsquerdaDistante, BaixoDistante, CimaDistante);
			Double[] Regra3 = inferenciaNebulosa.RegraObstaculoDiretaDistante(DireitaDistante);
			Double[] Regra4 = inferenciaNebulosa.RegraObstaculoDiretaMedio(DireitaMedio);
			Double[] Regra5 = inferenciaNebulosa.RegraObstaculoDiretaPertoEObstaculoBaixoMedio(DireitaPerto, BaixoMedio);
			Double[] Regra6 = inferenciaNebulosa.RegraObstaculoDiretaPertoEObstaculoBaixoDistanteEObstaculoCimaPerto(DireitaPerto, BaixoDistante, CimaPerto);
			Double[] Regra7 = inferenciaNebulosa.RegraObstaculoDiretaMedioEObstaculoBaixoMedioEObstaculoCimaDistante(DireitaMedio, BaixoMedio, CimaDistante);
			Double[] Regra8 = inferenciaNebulosa.RegraObstaculoDiretaPertoEObstaculoBaixoPertoEObstaculoCimaMedio(DireitaPerto, BaixoPerto, CimaMedio);
			Double[] Regra9 = inferenciaNebulosa
					.RegraObstaculoDiretaPertoEObstaculoBaixoPertoEObstaculoCimaMedioEObstaculoEsquerdaDistante(DireitaPerto, BaixoPerto, CimaMedio, EsquerdaDistante);
			Double[] Regra10 = inferenciaNebulosa
					.RegraObstaculoDiretaPertoEObstaculoBaixoPertoEObstaculoCimaMedioEObstaculoEsquerdaMedio(DireitaPerto, BaixoPerto, CimaMedio, EsquerdaMedio);
			Double[] Regra11 = inferenciaNebulosa.RegraObstaculoDireitaPertoEObstaculoBaixoMedioPreferenciaEsquerda(DireitaPerto, BaixoMedio);
			Double[] Regra12 = inferenciaNebulosa.RegraObstaculoEsquerdaMedioEObstaculoDireitaMedio(EsquerdaMedio, DireitaMedio);
			Double[] Regra13 = inferenciaNebulosa.RegraObstaculoDiretaPertoEObstaculoBaixoPertoEObstaculoEsquerdaPerto(DireitaPerto, EsquerdaPerto, BaixoPerto);
			Double[] Regra14 = inferenciaNebulosa.RegraObstaculoDireitaPertoEObstaculoBaixoMedioPreferenciaDireita(DireitaPerto, BaixoMedio);
			Double[] Regra15 = inferenciaNebulosa.RegraObstaculoDiretaDistanteESensorEsquerdaPerto(DireitaDistante, EsquerdaPerto);
			Double[] Regra16 = inferenciaNebulosa.RegraObstaculoDiretaPertoESensorBaixoPertoESensorCimaDistante(DireitaPerto, BaixoPerto, CimaDistante);
			Double[] Regra17 = inferenciaNebulosa.RegraObstaculoDiretaDistanteESensorBaixoPerto(DireitaDistante, BaixoPerto);
			Double[] Regra18 = inferenciaNebulosa.RegraObstaculoCimaPerto(CimaPerto);
			Double[] Regra19 = inferenciaNebulosa.RegraObstaculoEsquerdaPertoPrioridadeDireita(EsquerdaPerto);
			Double[] Regra20 = inferenciaNebulosa.RegraObstaculoBaixoPerto(BaixoPerto);
			Double[] Regra21 = inferenciaNebulosa.RegraObstaculoDiretaPertoEObstaculoCimaMedio(CimaMedio, DireitaPerto);
			Double[] Regra22 = inferenciaNebulosa.RegraObstaculoDiretaDistanteEObstaculoEsquerdaDistante(EsquerdaDistante, DireitaDistante);
			
			inferenciaNebulosaList.add(Regra1);
			inferenciaNebulosaList.add(Regra2);
			inferenciaNebulosaList.add(Regra3);
			inferenciaNebulosaList.add(Regra4);
			inferenciaNebulosaList.add(Regra5);
			inferenciaNebulosaList.add(Regra6);
			inferenciaNebulosaList.add(Regra7);
			inferenciaNebulosaList.add(Regra8);
			inferenciaNebulosaList.add(Regra9);
			inferenciaNebulosaList.add(Regra10);
			inferenciaNebulosaList.add(Regra11);
			inferenciaNebulosaList.add(Regra12);
			inferenciaNebulosaList.add(Regra13);
			inferenciaNebulosaList.add(Regra14);
			inferenciaNebulosaList.add(Regra15);
			inferenciaNebulosaList.add(Regra16);
			inferenciaNebulosaList.add(Regra17);
			inferenciaNebulosaList.add(Regra18);
			inferenciaNebulosaList.add(Regra19);
			inferenciaNebulosaList.add(Regra20);
			inferenciaNebulosaList.add(Regra21);
			inferenciaNebulosaList.add(Regra22);
			
			/*
			 * regra[2] == 1.0 -> Direita;
			 * regra[2] == 2.0 -> Baixo;
			 * regra[2] == 3.0 -> Cima;
			 * regra[2] == 4.0 -> Esquerda;
			 */
			for (Double[] regra : inferenciaNebulosaList) {
				if (regra[0] != 0.0) {
					if (regra[2] == 1.0) {
						
						adicionaDefuzzyficadores(defuzzyficador, regra[1], 8.0, 7.0, regra[0], regra[2]);
						direcaoDireita.somaDeterminanteDeDirecao(regra[2]);
						
					} else if (regra[2] == 2.0) {
						
						adicionaDefuzzyficadores(defuzzyficador, regra[1], 6.0, 5.0, regra[0], regra[2]);
						direcaoBaixo.somaDeterminanteDeDirecao(regra[2]);
						
					} else if (regra[2] == 3.0) {
						
						adicionaDefuzzyficadores(defuzzyficador, regra[1], 4.0, 3.0, regra[0], regra[2]);
						direcaoCima.somaDeterminanteDeDirecao(regra[2]);
						
					} else if (regra[2] == 4.0) {
						
						adicionaDefuzzyficadores(defuzzyficador, regra[1], 3.5, 2.5, regra[0], regra[2]);
						direcaoEsquerda.somaDeterminanteDeDirecao(regra[2]);
	
					}
				}
			}
			
			for (Double[] defuzzy : deffuzzyficadores) {
				somaDefuzzyficadores += (defuzzy[0] * defuzzy[1]);
				somaFuzzyficadores += defuzzy[0];
			}
			
			qtePassos = somaDefuzzyficadores / somaFuzzyficadores;
			
			if (direcaoDireita.getDetermiante() > direcaoBaixo.getDetermiante() && direcaoDireita.getDetermiante() > direcaoCima.getDetermiante() 
				&& direcaoDireita.getDetermiante() > direcaoEsquerda.getDetermiante()) {
				
				andarNoLabirinto(qtePassos, labirinto , 1.0);
				
			} else if (direcaoBaixo.getDetermiante() > direcaoDireita.getDetermiante() && direcaoBaixo.getDetermiante() > direcaoCima.getDetermiante()
					&& direcaoBaixo.getDetermiante() > direcaoEsquerda.getDetermiante()) {
				
				andarNoLabirinto(qtePassos, labirinto , 2.0);
				
			} else if (direcaoCima.getDetermiante() > direcaoDireita.getDetermiante() && direcaoCima.getDetermiante() > direcaoBaixo.getDetermiante()
					&& direcaoCima.getDetermiante() > direcaoEsquerda.getDetermiante()) {
				
				andarNoLabirinto(qtePassos, labirinto , 3.0);
			
			} else if (direcaoEsquerda.getDetermiante() > direcaoDireita.getDetermiante() && direcaoEsquerda.getDetermiante() > direcaoBaixo.getDetermiante()
					&& direcaoEsquerda.getDetermiante() > direcaoCima.getDetermiante()) {
				
				andarNoLabirinto(qtePassos, labirinto , 4.0);
				
			}
			
			if ((direcaoDireita.getDetermiante().equals(direcaoBaixo.getDetermiante()) && !direcaoDireita.getDetermiante().equals(0.0)) 
				|| (direcaoDireita.getDetermiante().equals(direcaoCima.getDetermiante()) && !direcaoDireita.getDetermiante().equals(0.0))  
				|| (direcaoDireita.getDetermiante().equals(direcaoEsquerda.getDetermiante()) && !direcaoDireita.getDetermiante().equals(0.0))) {
				
				andarNoLabirinto(qtePassos, labirinto , 1.0);
				
			} else if ((direcaoBaixo.getDetermiante().equals(direcaoCima.getDetermiante()) && !direcaoBaixo.getDetermiante().equals(0.0)) 
					|| (direcaoBaixo.getDetermiante().equals(direcaoEsquerda.getDetermiante()) && !direcaoBaixo.getDetermiante().equals(0.0))) {
				
				andarNoLabirinto(qtePassos, labirinto , 2.0);
				
			} else if (direcaoCima.getDetermiante().equals(direcaoEsquerda.getDetermiante()) && !direcaoCima.getDetermiante().equals(0.0)) {
				
				andarNoLabirinto(qtePassos, labirinto , 3.0);
				
			}
			
			imprimeLabirinto(labirinto);
			
			deffuzzyficadores = new ArrayList<Double[]>();
			inferenciaNebulosaList = new ArrayList<Double[]>();
			
		}
		
	}
	
	private static void adicionaDefuzzyficadores(Passo defuzzyficador, Double regra, Double longo, Double pouco, Double inferencia, Double direcao) {
		Double[] auxiliar = new Double[3];
		auxiliar[2] = direcao;
		if (regra.equals(longo)) {
			auxiliar[0] = inferencia;
			auxiliar[1] = defuzzyficador.Longo(inferencia);
			deffuzzyficadores.add(auxiliar);
		} else if (regra.equals(pouco)) {
			auxiliar[0] = inferencia;
			auxiliar[1] = defuzzyficador.Pouco(inferencia);
			deffuzzyficadores.add(auxiliar);
		}
		
	}
	
	private static String[][] preencheLabirinto(int saida) {
		String labirinto[][] = new String[41][41];
		
		for (int i = 0 ; i < 40 ; i++) {
			for (int j = 0 ; j < 40 ; j++) {
				labirinto[i][j] = "-";
			}
		}
		
		for (int i = 0 ; i < 41 ; i++) {
			labirinto[0][i] = "L";
			labirinto[40][i] = "L";
			labirinto[i][40] = "L";
			labirinto[i][0] = "L";
		}
		
		labirinto[saida][39] = "S";
		labirinto[1][1] = "O";
		
		for (int i = 0 ; i < 5 ; i++) {
			geraObstaculos(labirinto);
		}

		return labirinto;
	}
	
	private static void imprimeLabirinto(String[][] labirinto) {
		for (int l = 0; l < labirinto.length; l++)  {  
			for (int c = 0; c < labirinto[0].length; c++)     { 
				System.out.print(labirinto[l][c] + " ");
	       	}  
	       	System.out.println(" ");
		}
		System.out.println("                                                                                                 ");
		System.out.println("*************************************************************************************************");
		System.out.println("*************************************************************************************************");
		System.out.println("                                                                                                 ");
	}
	
	private static String[][] geraObstaculos(String[][] labirinto) {
		
		int colunaL = geraAleatorio(40, 1);
		int linhaL = geraAleatorio(40, 1);
		
		while (existeObstaculo(labirinto, linhaL, colunaL)) {
			colunaL = geraAleatorio(40, 1);
			linhaL = geraAleatorio(40, 1);
		}
		
		return montaObstaculoL(colunaL, linhaL, labirinto);
		
	}
	
	private static String[][] montaObstaculoL(int colunaL, int linhaL, String[][] labirinto) {
		
		labirinto[linhaL][colunaL] = "L";
		labirinto[linhaL+1][colunaL] = "L";
		labirinto[linhaL+1][colunaL+1] = "L";
		
		return labirinto;
	}
	
	private static Boolean existeObstaculo(String[][] labirinto, int linha, int coluna) {
		return 	labirinto[linha][coluna] == "L" ||
				labirinto[linha+1][coluna] == "L" ||
				labirinto[linha+1][coluna+1] == "L" ||
				labirinto[linha][coluna] == "O" ||
				labirinto[linha+1][coluna] == "O" ||
				labirinto[linha+1][coluna+1] == "O" ||
				labirinto[linha][coluna] == "S" ||
				labirinto[linha+1][coluna] == "S" ||
				labirinto[linha+1][coluna+1] == "S";
	}
	
	private static String[][] andarNoLabirinto(Double qtePassos, String[][] labirinto, Double direcao) {
		int linha = 0, coluna = 0;
		for ( int i = 0; i < 41; ++i ) {
		    for ( int j = 0; j < 41; ++j ) {
		        if ( labirinto[i][j] == "O" ) {
		            linha = i;
		            coluna = j;
		        } 
		    }
		}
		
		if (sensorDireita.identificaSaida() || sensorBaixo.identificaSaida() || sensorCima.identificaSaida() || sensorEsquerda.identificaSaida()) {
			for ( int i = 0; i < 41; ++i ) {
			    for ( int j = 0; j < 41; ++j ) {
			        if ( labirinto[i][j] == "O" ) {
			            linha = i;
			            coluna = j;
			        } 
			    }
			}
			labirinto[linha][coluna] = "-";
			imprimeLabirinto(labirinto);
			achouSaida = true;
			direcao = 0.0;
		}
		
		
		if (direcao.equals(1.0) && !((coluna + Math.floor(qtePassos)) >= 40)) {
			
			if (labirinto[linha][(int) (coluna + Math.floor(qtePassos))] != "L") {
				
				labirinto[linha][coluna] = "-";
				labirinto[linha][(int) (coluna + Math.floor(qtePassos))] = "O";
				
				
			} else {
				direcao = achaOutraDirecao(direcao);
			}
			
		} else if ((direcao.equals(1.0) && (coluna + Math.floor(qtePassos)) >= 40)) {
			direcao = achaOutraDirecao(direcao);
		}
		
		if (direcao.equals(2.0) && !((linha + Math.floor(qtePassos)) >= 40)) {
			
			if (labirinto[(int) (linha + Math.floor(qtePassos))][coluna] != "L") {
				
				labirinto[linha][coluna] = "-";
				labirinto[(int) (linha + Math.floor(qtePassos))][coluna] = "O";
				
			} else {
				direcao = achaOutraDirecao(direcao);
			}
			
		} else if ((direcao.equals(2.0) && direcao.equals(2.0) && (linha + Math.floor(qtePassos)) >= 40)) {
			direcao = achaOutraDirecao(direcao);
		}
		
		if (direcao.equals(3.0) && !((linha - Math.floor(qtePassos)) <= 0)) {
			
			if (labirinto[(int) (linha - Math.floor(qtePassos))][coluna] != "L") {
				
				labirinto[linha][coluna] = "-";
				labirinto[(int) (linha - Math.floor(qtePassos))][coluna] = "O";
				
			} else {
				direcao = achaOutraDirecao(direcao);
			}
			
		} else if ((direcao.equals(3.0) && direcao.equals(3.0) && (linha - Math.floor(qtePassos)) <= 0)) {
			direcao = achaOutraDirecao(direcao);
		}
		
		if (direcao.equals(4.0) && !((coluna - Math.floor(qtePassos)) <= 0)) {
			
			if (labirinto[linha][(int) (coluna - Math.floor(qtePassos))] != "L") {
				
				labirinto[linha][coluna] = "-";
				labirinto[linha][(int) (coluna - Math.floor(qtePassos))] = "O";
				
			} else {
				direcao = achaOutraDirecao(direcao);
			}

			
		} else if ((direcao.equals(4.0) && direcao.equals(4.0) && (coluna - Math.floor(qtePassos)) <= 0)) {
			direcao = achaOutraDirecao(direcao);
		}

		return labirinto;
	}
	
	private static int geraAleatorio(int max, int min) {
        Random random = new Random();
        return (random.nextInt(max - (min - 1)) + min);
    }
	
	private static Double achaOutraDirecao(Double direcao) {
		for (Double [] def : deffuzzyficadores) {
			if (direcao != def[2]) {
				return def[2];
			}
		}
		return direcao;
	}
	
}
