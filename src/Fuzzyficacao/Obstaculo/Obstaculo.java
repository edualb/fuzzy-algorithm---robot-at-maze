package Fuzzyficacao.Obstaculo;

public class Obstaculo {
	
	public Double Perto(int distancia) {
		if (distancia <= 1) {
			return 1.0;
		} else if (distancia > 1 && distancia < 4) {
			return (4.0 - distancia) / (4-1);
		}
		return 0.0;
	}
	
	public Double Medio(int distancia) {
		if (distancia > 1 && distancia < 3) {
			return (distancia - 1.0) / (3-1);
		} else if (distancia == 3) {
			return 1.0;
		} else if (distancia > 3 && distancia < 5) {
			return (5.0 - distancia) / (5-3);
		}
		return 0.0;
	}
	
	public Double Distante(int distancia) {
		if (distancia > 3 && distancia < 4) {
			return (distancia - 3.0) / (4-3);
		} else if (distancia >= 4) {
			return 1.0;
		}
		return 0.0;
	}

}
