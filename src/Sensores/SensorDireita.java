package Sensores;

public class SensorDireita implements Sensor{
	
	String labirinto[][] = new String[41][41];

	public SensorDireita(String[][] labirinto) {
		setLabirinto(labirinto);
	}

	public String[][] getLabirinto() {
		return labirinto;
	}

	public void setLabirinto(String[][] labirinto) {
		this.labirinto = labirinto;
	}
	
	@Override
	public int identificaObstaculo() {
		int linha = 0, coluna = 0;
		for ( int i = 0; i < 41; ++i ) {
		    for ( int j = 0; j < 41; ++j ) {
		        if ( labirinto[i][j] == "O" ) {
		            linha = i;
		            coluna = j;
		        } 
		    }
		}
		
		if (labirinto[linha][coluna+1] == "L") {
			return 1;
		} else if (labirinto[linha][coluna+2] == "L") {
			return 2;
		} else if (labirinto[linha][coluna+3] == "L") {
			return 3;
		} else if (labirinto[linha][coluna+4] == "L") {
			return 4;
		}
		
		return 5;
	}
	
	@Override
	public Boolean identificaSaida() {
		int linha = 0, coluna = 0;
		for ( int i = 0; i < 41; ++i ) {
		    for ( int j = 0; j < 41; ++j ) {
		        if ( labirinto[i][j] == "O" ) {
		            linha = i;
		            coluna = j;
		        } 
		    }
		}
		
		if (!((coluna+1) > 40)) {
			if (labirinto[linha][coluna+1] == "S") {
				return true;
			}
		}
		
		if (!((coluna+2) > 40)) {
			if (labirinto[linha][coluna+2] == "S") {
				return true;
			}
		}
		
		if (!((coluna+3) > 40)) {
			if (labirinto[linha][coluna+3] == "S") {
				return true;
			}
		}
		
		if (!((coluna+4) > 40)) {
			if (labirinto[linha][coluna+4] == "S") {
				return true;
			}
		}

		if (!((coluna+5 > 40))) {
			if (labirinto[linha][coluna+5] == "S") {
				return true;
			}
		}
		
		return false;
	}

}
