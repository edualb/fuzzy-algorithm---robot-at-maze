package Sensores;

public class SensorBaixo implements Sensor{
	
	String labirinto[][] = new String[41][41];

	public SensorBaixo(String[][] labirinto) {
		setLabirinto(labirinto);
	}

	public String[][] getLabirinto() {
		return labirinto;
	}

	public void setLabirinto(String[][] labirinto) {
		this.labirinto = labirinto;
	}

	@Override
	public int identificaObstaculo() {
		int linha = 0, coluna = 0;
		
		for ( int i = 0; i < 41; ++i ) {
		    for ( int j = 0; j < 41; ++j ) {
		        if ( labirinto[i][j] == "O" ) {
		            linha = i;
		            coluna = j;
		        } 
		    }
		}
		
		if (labirinto[linha+1][coluna] == "L") {
			return 1;
		} else if (labirinto[linha+2][coluna] == "L") {
			return 2;
		} else if (labirinto[linha+3][coluna] == "L") {
			return 3;
		} else if (labirinto[linha+4][coluna] == "L") {
			return 4;
		}
		
		return 5;
	}

	@Override
	public Boolean identificaSaida() {
		int linha = 0, coluna = 0;
		for ( int i = 0; i < 41; ++i ) {
		    for ( int j = 0; j < 41; ++j ) {
		        if ( labirinto[i][j] == "O" ) {
		            linha = i;
		            coluna = j;
		        } 
		    }
		}
		
		if (!((linha+1) > 40)) {
			if (labirinto[linha+1][coluna] == "S") {
				return true;
			}
		}
		
		if (!((linha+2) > 40)) {
			if (labirinto[linha+2][coluna] == "S") {
				return true;
			}
		}
		
		if (!((linha+3) > 40)) {
			if (labirinto[linha+3][coluna] == "S") {
				return true;
			}
		}
		
		if (!((linha+4) > 40)) {
			if (labirinto[linha+4][coluna] == "S") {
				return true;
			}
		}

		if (!((linha+5 > 40))) {
			if (labirinto[linha+5][coluna] == "S") {
				return true;
			}
		}
		
		return false;
	}

}
