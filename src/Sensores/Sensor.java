package Sensores;

public interface Sensor {
	
	public int identificaObstaculo();
	public Boolean identificaSaida();

}
